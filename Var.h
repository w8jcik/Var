#pragma once

#include <string>

#include <string.h>
#include <assert.h>

template <typename T>
class Var {
  int layers_count = 0;
  int enabled_layers = 0x00000000;  // bit encoded
  T* layers_values;
  T value;
  T observed_value;
  std::string name;
  void (*observer)(Var&, T) = nullptr;
  void (*logger)(const char*) = nullptr;

  T resolve_value() {
    if (this->enabled_layers > 0) {
      for (int i = (this->layers_count) - 1; i > 0; i -= 1) {
        if ((this->enabled_layers >> i) & 1) {
          return this->layers_values[i];
        }
      }
    }

    return this->layers_values[0];
  }

public:
  Var(T value) : name("") {
    this->set_layers_count(2);
    this->set(value);
  }

  ~Var(void) {
    if (this->layers_count != 0) {
      delete[] this->layers_values;
    }
  }

  void set_layers_count(int new_layers_count) {
    if (this->layers_count > 0) {
      delete[] this->layers_values;
    }

    if (new_layers_count > 0) {
      this->layers_values = new T[new_layers_count];
    }

    this->layers_count = new_layers_count;
  }

  void set(int layer, T new_value) {
    assert(layer < this->layers_count);

    this->layers_values[layer] = new_value;

    if (layer > 0) {
      this->enabled_layers |= 1 << layer;
    }

    this->value = this->resolve_value();

    if (this->observer) {
      if (this->value != this->observed_value) {
        this->observer(*this, this->value);
        this->observed_value = this->value;
      }
    }
  }

  void set(T new_value) {
    this->set(0, new_value);
  }

  void override(T new_value) {
    this->set(1, new_value);
  }

  void release(int layer) {
    this->enabled_layers &= ~(1 << layer);
    this->value = this->resolve_value();

    if (this->observer) {
      if (this->value != this->observed_value) {
        this->observer(*this, this->value);
        this->observed_value = this->value;
      }
    }
  }

  void release() {
    this->release(1);
  }

  T get(void) {
    return this->value;
  }

  T get(int layer) {
    return this->layers_values[layer];
  }

  int is_overriden() {
    return (this->enabled_layers > 0);
  }

  void set_name(std::string new_name) {
    this->name = new_name;
  }

  std::string get_name(void) {
    if (this->name.length()) {
      return this->name;
    } else {
      if (this->logger) {
        this->logger("Var.get_name was called on a variable without a name");
      }
      return "unnamed";
    }
  }

  void set_observer(void (*new_observer)(Var& self, T value)) {
    this->observer = new_observer;

    if (this->observer) {
      this->observer(*this, this->value);
      this->observed_value = this->value;
    }
  }

  void trigger_observer() {
    if (this->observer) {
      this->observer(*this, this->value);
    }
  }

  void trigger_observer(T value) {
    if (this->observer) {
      this->observer(*this, value);
    }
  }

  void set_logger(void (*new_logger)(const char* message)) {
    this->logger = new_logger;
  }
};

template <typename T>
class VarWithTime : public Var<T> {
  int changed_time;

public:
  int get_changed_time(void) {
    return this->changed_time;
  }

  void set_changed_time(T new_time) {
    this->changed_time = new_time;
  }
};
