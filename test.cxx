#include <stdio.h>
#include <assert.h>

#include <string>
#include <iostream>

#include "Var.h"

enum {
  PRI_NORMAL,
  PRI_HIGHER,
  PRI_HIGHEST
};

int transmitted_count = 0;

void transmit(Var<int>& self, int new_value) {
  transmitted_count += 1;
}

typedef enum { red, green, blue } colors_t;

int main() {
  Var<int> valve(0);

  std::cout << "Creating layers" << std::endl;

  valve.set_layers_count(2);

  std::cout << "Lower layer should store data" << std::endl;

  valve.set(5);
  assert(valve.get() == 5);

  std::cout << "Higher layer should store data" << std::endl;

  valve.set(PRI_HIGHER, 10);
  assert(valve.get() == 10);

  std::cout << "Lower layer should be ignored" << std::endl;

  valve.set(20);
  assert(valve.get() == 10);

  std::cout << "Lower layer should be used after higher layer is released" << std::endl;

  valve.release(PRI_HIGHER);
  assert(valve.get() == 20);

  std::cout << "Observer should be triggered when assigned" << std::endl;

  valve.set_observer(transmit);

  assert(transmitted_count == 1);

  std::cout << "Observer should be triggered only when value changes" << std::endl;

  valve.set(5);
  assert(valve.get() == 5);

  valve.set(PRI_HIGHER, 10);
  assert(valve.get() == 10);

  valve.set(20);
  assert(valve.get() == 10);

  valve.set(20);
  assert(valve.get() == 10);

  valve.release(PRI_HIGHER);
  assert(valve.get() == 20);

  assert(transmitted_count == 4);

  std::cout << "Observer disabled before additional operations" << std::endl;

  valve.set_observer(NULL);

  std::cout << "Three layers should respect their priorities" << std::endl;

  valve.set_layers_count(3);

  valve.set(8);
  valve.set(PRI_HIGHEST, 20);
  valve.set(PRI_HIGHER, 4);

  assert(valve.get() == 20);

  valve.release(PRI_HIGHER);

  assert(valve.get() == 20);

  valve.set(PRI_HIGHER, 4);

  valve.release(PRI_HIGHEST);

  assert(valve.get() == 4);

  valve.release(PRI_HIGHER);

  assert(valve.get() == 8);

  std::cout << "Operations on three layers should not trigger observer since it was disabled" << std::endl;

  assert(transmitted_count == 4);

  std::cout << "Unnamed variable should give `unnamed` when asked for name" << std::endl;

  assert(valve.get_name() == "unnamed");

  std::cout << "Named variable should give name that was given to it" << std::endl;

  valve.set_name("valve");
  assert(valve.get_name() == "valve");

  std::cout << "Variable should report if it is overriden on any layer higher then base one" << std::endl;

  assert(!valve.is_overriden());
  valve.set(PRI_HIGHER, 5);
  assert(valve.is_overriden());

  std::cout << "Enum should be accepted as a data type" << std::endl;

  Var<colors_t> light(green);
  assert(light.get() == green);

  light.set(red);
  assert(light.get() == red);

  return 0;
}
