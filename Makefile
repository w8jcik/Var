shared_flags = -Wall -pedantic -lstdc++ -Og -g

help:
	@echo ""
	@echo "  make tests  -- compile and execute tests for the Var class"
	@echo ""

all:
	@make -s tests

clean:
	rm -f test.o

tests:
	g++ -std=c++11 $(shared_flags) test.cxx -o test.o && ./test.o | sed 's/.*/C++11: &/' && echo ""
	g++ -std=c++14 $(shared_flags) test.cxx -o test.o && ./test.o | sed 's/.*/C++14: &/' && echo ""
	g++ -std=c++17 $(shared_flags) test.cxx -o test.o && ./test.o | sed 's/.*/C++17: &/' && echo ""
	@make -s clean
