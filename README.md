# Var

A variable with layers.

Values on different layers can be set by different parts of your program, for example different threads or interrupt handlers. Higher layers are always more important. When higher layer is set, decisions performed on lower layers are surpressed but still stored. If higher layer is released, the variable receives a value of one of the lower layers.

## Example

```c++
Var speed;

enum { LAYER_NORMAL, LAYER_HIGHER };

void speed_handler(int new_value) {
  // set some GPIO or transmit a command over a serial connection
}

int main() {
  speed.set(25);
  speed.set_observer(speed_handler);
  
  speed.set(LAYER_HIGHER, 50);
  assert(speed.get() == 50);
  
  speed.release(LAYER_HIGHER);
  assert(speed.get() == 25);
}
```

## Real-world example

* a GUI controls when a mixer should start and stop according to a time shedule and a few sensors
* if the engine is overloaded or somebody is putting hands into forbidden area it should stop
* there are hidden buttons that allow technical personnel to control everything manually for diagnostic purposes

The service buttons set the highest layer in their interrupt handlers. One of the buttons is a master button, if released, the layer is released and the device restores normal operation. It will not restore normal operation if the middle layer responsible for malfunction and safety detection is set.

## API

`set_layers_count(int new_layers_count)` – by default two layers are created

`set(int value)`

* sets a value on the basic layer  
* you should use it to initialize your variables just at the beginning

`set(int layer, int value)`  

* sets a value on a selected layer, 0 means the basic layer  
* you can use plain numbers, or some enum
    * for the default two layers, it can be `enum { LAYER_NORMAL, LAYER_HIGHER };`
        * `set(LAYER_NORMAL, 5)` equals `set(5)`

`override(int value)` – an alias to `set`, sets the second layer

`int get()` – returns a value that takes into consideration all the layers

`release(int layer)` – releases a layer

`release()` – releases the second layer

`int is_overriden()` – returns `true` if any of the higher layers are active

`set_name(const char* new_name)` – name can be assigned to the variable

`const char* get_name(void)` – and retrieved later

`set_observer(void (*new_observer)(int value))`

* you can assign a procedure that will fire when the value changes, it receives the value as an argument
* the assigned procedure fires when you `set` or `release` but only when the computed value changes
    * it also fires instantly when you `set_observer`, so you should `set` an initial value first

`set_logger(void (*new_logger)(const char* message))`

* you can assign a procedure that will fire on non-critical errors, it receives a human-readable message

# Tests

```bash
$ make tests
```
